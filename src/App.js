import React, { useState } from 'react';
import { ReactComponent as Unrated } from './unrated.svg';
import { ReactComponent as Rated } from './rated.svg';
import styled from 'styled-components';
import './App.css';

function App() {
  const [value, setValue] = useState('50');
  const [isFixed, setIsFixed] = useState(false);

  const App = styled.div`
      display: grid;
      justify-items: center;
      align-content: center;
      height: 100vh;
      width: 100vw;
      overflow: hidden;
  `;

  const StyledUnrated = styled(Unrated)`
      width: 100%;
      height: 100%;
  `

  const StyledRated = styled(Rated)`
      width: 100%;
      height: 100%;
  `

  const ClipWrapper = styled.div`
    position: absolute;
    top: 0;
    clip-path: ${props => props && props.clipPath};
  `;

  const Wrapper = styled.div`
      display: block;
      width: 120px;
      position: relative;
      filter: ${props => props.dropShadow || ''} ;
  `;

  return (
    <App>
      <Wrapper
        onMouseMove={e => {
          if(!isFixed){
            const { left } = e.currentTarget.getBoundingClientRect();
            const x = Math.round(Math.floor(
              (e.clientX - left) / (e.currentTarget.offsetWidth / 100)
              ) / 10) * 10;
              setValue(x);
            }
        }}
        onClick={e=>setIsFixed(!isFixed)}
        dropShadow={isFixed ? "drop-shadow(0 0 10px #FFD600)": ""}
      >
        <StyledUnrated />
        <ClipWrapper
          clipPath={`polygon(0 0, ${value}% 0, ${value}% 100%, 0 100%)`}
          
        >
          <StyledRated />
        </ClipWrapper>
      </Wrapper>
      <div>
        {
          5 * value / 100
        }
        </div>
    </App>
  );
}

export default App;
